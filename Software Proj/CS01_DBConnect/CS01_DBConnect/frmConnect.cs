﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace CS01_DBConnect
{
    public partial class frmConnect : Form
    {
        OracleConnection conn = new OracleConnection(DBConnect.oradb);
        public frmConnect()
        {
            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            
        }

        private void frmConnect_Load(object sender, EventArgs e)
        {
            try
            {
                //Use get Stock
                Stock MyStock = new Stock();

                DataSet myDS = new DataSet();
                //load Data onto form
                grdData.DataSource = MyStock.getStock(myDS).Tables["stock"];
                //CLose Database
            
            }
            catch (OracleException ex) {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
