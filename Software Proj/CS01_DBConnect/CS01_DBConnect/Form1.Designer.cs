﻿namespace CS01_DBConnect
{
    partial class frmStockUpd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.grdData = new System.Windows.Forms.DataGridView();
            this.grpUpdate = new System.Windows.Forms.GroupBox();
            this.Description = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDesc_Upd = new System.Windows.Forms.TextBox();
            this.txtCP_Upd = new System.Windows.Forms.TextBox();
            this.txtSP_Upd = new System.Windows.Forms.TextBox();
            this.txtQty_Upd = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.grpUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Description";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(90, 32);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.PasswordChar = '┘';
            this.txtSearch.Size = new System.Drawing.Size(261, 20);
            this.txtSearch.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(160, 67);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(112, 29);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // grdData
            // 
            this.grdData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdData.Location = new System.Drawing.Point(27, 112);
            this.grdData.Name = "grdData";
            this.grdData.Size = new System.Drawing.Size(373, 111);
            this.grdData.TabIndex = 3;
            this.grdData.Visible = false;
            // 
            // grpUpdate
            // 
            this.grpUpdate.Controls.Add(this.btnUpdate);
            this.grpUpdate.Controls.Add(this.txtQty_Upd);
            this.grpUpdate.Controls.Add(this.txtSP_Upd);
            this.grpUpdate.Controls.Add(this.txtCP_Upd);
            this.grpUpdate.Controls.Add(this.txtDesc_Upd);
            this.grpUpdate.Controls.Add(this.label5);
            this.grpUpdate.Controls.Add(this.label4);
            this.grpUpdate.Controls.Add(this.label3);
            this.grpUpdate.Controls.Add(this.Description);
            this.grpUpdate.Location = new System.Drawing.Point(27, 243);
            this.grpUpdate.Name = "grpUpdate";
            this.grpUpdate.Size = new System.Drawing.Size(372, 277);
            this.grpUpdate.TabIndex = 4;
            this.grpUpdate.TabStop = false;
            this.grpUpdate.Text = "Update Stock Info";
            this.grpUpdate.Visible = false;
            // 
            // Description
            // 
            this.Description.AutoSize = true;
            this.Description.Location = new System.Drawing.Point(18, 33);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(60, 13);
            this.Description.TabIndex = 0;
            this.Description.Text = "Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Cost Price";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Sale Price";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Quantity";
            // 
            // txtDesc_Upd
            // 
            this.txtDesc_Upd.Location = new System.Drawing.Point(84, 30);
            this.txtDesc_Upd.Name = "txtDesc_Upd";
            this.txtDesc_Upd.Size = new System.Drawing.Size(261, 20);
            this.txtDesc_Upd.TabIndex = 4;
            // 
            // txtCP_Upd
            // 
            this.txtCP_Upd.Location = new System.Drawing.Point(84, 72);
            this.txtCP_Upd.Name = "txtCP_Upd";
            this.txtCP_Upd.Size = new System.Drawing.Size(261, 20);
            this.txtCP_Upd.TabIndex = 5;
            // 
            // txtSP_Upd
            // 
            this.txtSP_Upd.Location = new System.Drawing.Point(84, 115);
            this.txtSP_Upd.Name = "txtSP_Upd";
            this.txtSP_Upd.Size = new System.Drawing.Size(261, 20);
            this.txtSP_Upd.TabIndex = 6;
            // 
            // txtQty_Upd
            // 
            this.txtQty_Upd.Location = new System.Drawing.Point(84, 160);
            this.txtQty_Upd.Name = "txtQty_Upd";
            this.txtQty_Upd.Size = new System.Drawing.Size(261, 20);
            this.txtQty_Upd.TabIndex = 7;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(133, 208);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(112, 29);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "Search";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // frmStockUpd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 532);
            this.Controls.Add(this.grpUpdate);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label1);
            this.Name = "frmStockUpd";
            this.Text = "Update Stock";
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.grpUpdate.ResumeLayout(false);
            this.grpUpdate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView grdData;
        private System.Windows.Forms.GroupBox grpUpdate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtQty_Upd;
        private System.Windows.Forms.TextBox txtSP_Upd;
        private System.Windows.Forms.TextBox txtCP_Upd;
        private System.Windows.Forms.TextBox txtDesc_Upd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Description;
    }
}