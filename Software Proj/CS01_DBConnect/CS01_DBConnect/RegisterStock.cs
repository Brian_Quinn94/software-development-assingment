﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace CS01_DBConnect
{

    public partial class frmRegStock : Form
    {
        public frmRegStock()
        {
            InitializeComponent();
        }

        private void RegisterStock_Load(object sender, EventArgs e)
        {
            txtStockNo.Text = Stock.nextStockNo().ToString("00000");
        }



        private void btnReg_Click(object sender, EventArgs e)
        {
            Stock myStock = new Stock(Convert.ToInt32(txtStockNo.Text), txtDescription.Text, Convert.ToDecimal(txtCostPrice.Text), Convert.ToDecimal(txtSalesPrice.Text), Convert.ToInt32(txtQty.Text));
            myStock.regStock();
            MessageBox.Show("Stock" + txtStockNo.Text + "Registered", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtDescription.Text = "";
            txtCostPrice.Text = "";
            txtSalesPrice.Text = "";
            txtQty.Text = "";

            txtStockNo.Text = Stock.nextStockNo().ToString("00000");
            txtDescription.Focus();

            /*myStock.setStockNo(Convert.ToInt32(txtStockNo.Text));
            myStock.setDescription(txtDescription.Text);
            myStock.setCostPrice(Convert.ToDecimal(txtCostPrice.Text));
            myStock.setSalePrice(Convert.ToDecimal(txtSalesPrice.Text));
            myStock.setQty(Convert.ToInt32(txtQty.Text));
            myStock.setStatus('R');

            myStock.regStock();

            MessageBox.Show("Stock" + txtStockNo.Text + "Registered", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);

            txtDescription.Text = "";
            txtCostPrice.Text = "";
            txtSalesPrice.Text = "";
            txtQty.Text = "";

            txtStockNo.Text = Stock.nextStockNo().ToString("00000");
            txtDescription.Focus();*/



        }
    }
}
