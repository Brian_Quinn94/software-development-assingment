﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace CS01_DBConnect
{
    class Stock
    {
        //Instance variables
        private int stock_No;
        private string description;
        private decimal cost_Price;
        private decimal sale_Price;
        private int qty;
        private char status;

        public Stock(){
            stock_No = 0;
            description = "";
            cost_Price = 0;
            sale_Price = 0;
            status = ' ';
        }

        public DataSet getStock(DataSet ds) {

            //OpenConnection
            OracleConnection conn = new OracleConnection(DBConnect.oradb);
            conn.Open();

            //Define SQL query
            string strSQL = "SELECT Stock_No, Description FROM Stock";
            OracleCommand cmd = new OracleCommand(strSQL, conn);

            //Execute the query
            OracleDataAdapter da = new OracleDataAdapter(cmd);
           
            da.Fill(ds, "stock");
           
            //CLose Database
            conn.Close();

            return ds;

        }

        public Stock(int Stock_No, String Description, decimal Cost_Price, decimal Sale_Price, int Qty)
        {
            stock_No = Stock_No;
            description = Description;
            cost_Price = Cost_Price;
            sale_Price = Sale_Price;
            qty = Qty;
            status = 'R';
        }

        public static int nextStockNo() {

            int intNextStockNo;

            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();

            String strlSQL = "SELECT MAX (stock_No) From Stock";

            OracleCommand cmd = new OracleCommand(strlSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.IsDBNull(0))
                intNextStockNo = 1;
            else
                intNextStockNo = Convert.ToInt16(dr.GetValue(0)) + 1;

            myConn.Close();
            return intNextStockNo;
        }

        public void regStock()
        {

            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            String strSQL = "Insert INTO stock Values(" + this.stock_No + ",'" + this.description.ToUpper() + "'," + this.cost_Price + "," + this.sale_Price + "," + this.qty + ",'" + this.status + "')";


            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();

            myConn.Close();

        }



    }
}
