DROP TABLE STOCK;
DROP TABLE CLIENT;
DROP TABLE ORDERS;

--Create Table Stock
CREATE TABLE STOCK
(Stock_No numeric(4),
  Description char(20),
  Part_Type char(3),
  Cost_Price numeric(5,2),
  Sale_Price numeric(5,2),
  Qty numeric(3),
  CONSTRAINT pk_Stock PRIMARY KEY (Stock_No)
  --CONSTRAINT fk FOREIGN KEY
  );
  
  
--Create Table Client

CREATE TABLE CLIENT
(Customer_ID numeric (4),
  Forename char(20),
  Surname char(20),
  DOB date,
  Address char (40), 
  CONSTRAINT pk_Client PRIMARY KEY (Customer_ID)
  --CONSTRAINT fk FOREIGN KEY
  );
  
 --Create Table ORDERS 
CREATE TABLE ORDERS
(Order_No numeric (4),
  Order_Date date,
  Due_Date date,
  Dispactched char(1), 
  CONSTRAINT pk_ORDERS PRIMARY KEY (Order_No)
  --CONSTRAINT fk FOREIGN KEY
  );
  
--Tempoary table info
INSERT INTO Stock
VALUES(1,'Nvidia gtx 1080','GPU',200,325,20);
INSERT INTO Stock
VALUES(2,'Nvidia gtx 960','GPU',150,200,40);
INSERT INTO CLIENT
VALUES(1 ,'Doe','John','09-JUN-25','10 Mainstreet');
INSERT INTO CLIENT
VALUES(2 ,'Jack','Simmons','01-MAR-16','18 Portstreet');
INSERT INTO ORDERS
VALUES(1,'12-MAR-12','16-APR-24','Y');
INSERT INTO ORDERS
VALUES(2,'12-JUN-12','16-JUL-24','N');

COMMIT;