﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;


namespace PcClinic
{
    class Stock
    {
        private int stock_No;
        private string description;
        private string part_Type;
        private float cost_Price;
        private float sales_Price;
        private int qty;
        public Stock()
        {
            stock_No = 0;
            description = "";
            part_Type = "";
            cost_Price = 0;
            sales_Price = 0;
            qty = 0;
        }

        public DataSet getStock(DataSet ds)
        {

            //Open Connection
            OracleConnection conn = new OracleConnection(DBConnect.oradb);
            conn.Open();
            string strSQL = "SELECT Stock_No, Description FROM Stock";
            OracleCommand cmd = new OracleCommand(strSQL, conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(ds, "stock");
            conn.Close();

            return ds;

        }
        public Stock(int Stock_No, String Description, string Part_Type, float Cost_Price, float Sale_Price, int Qty)
        {
            stock_No = Stock_No;
            description = Description;
            part_Type = Part_Type;
            cost_Price = Cost_Price;
            sales_Price = Sale_Price;
            qty = Qty;

        }
        public static int nextStockNo()
        {

            int intNextStockNo;

            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();

            String strlSQL = "SELECT MAX (stock_No) From Stock";

            OracleCommand cmd = new OracleCommand(strlSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.IsDBNull(0))
                intNextStockNo = 1;
            else
                intNextStockNo = Convert.ToInt16(dr.GetValue(0)) + 1;

            myConn.Close();
            return intNextStockNo;
        }

        public void regStock()
        {

            OracleConnection myConn = new OracleConnection();
            myConn.Open();
            String strSQL = "INSERT INTO Stock VALUES(" + this.stock_No + ",'" + this.description.ToUpper() + "'," +
                    ",'" + this.part_Type + "'," + this.cost_Price + "'" + this.sales_Price + "," + this.qty + "')";

            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();
            myConn.Close();

        }

        public void UpdateStock()
        {
            /*
           OracleConnection myConn = new OracleConnection();
           myConn.Open();
           //insert strSQL here
           OracleCommand cmd = new OracleCommand(strSQL, myConn);
           cmd.ExecuteNonQuery();
           myConn.Close();
           */
        }


    }
}