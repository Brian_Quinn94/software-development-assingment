﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using PcClinic.Clients;
using PcClinic.Orders;
using PcClinic.Stock;

namespace PcClinic
{
    public partial class frmPcClinic : Form
    {
        frmPcClinic parent;
        public frmPcClinic()
        {
            InitializeComponent();
        }
        public frmPcClinic(frmPcClinic Parent) {

            InitializeComponent();
            parent = Parent;

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void mnuRegStock_Click(object sender, EventArgs e)
        {
           frmRegStock frmNext = new frmRegStock(this);
           this.Hide();
           frmNext.Show();
        }

        private void mnuUpdateStock_Click(object sender, EventArgs e)
        {
          frmUpdateStock frmNext = new frmUpdateStock(this);
            this.Hide();
            frmNext.Show();
        }


        

        private void mnuRegClient_Click(object sender, EventArgs e)
        {
         frmRegClient frmNext = new frmRegClient(this);
           this.Hide();
           frmNext.Show();
        }

        private void mnuUpdateClient_Click(object sender, EventArgs e)
        {
           frmUpdateClient frmNext = new frmUpdateClient(this);
           this.Hide();
           frmNext.Show();
        }

   
        

        private void mnuNewOrder_Click(object sender, EventArgs e)
        {
          frmNewOrder frmNext = new frmNewOrder(this);
          this.Hide();
          frmNext.Show();
        }

        private void mnuCancelOrder_Click(object sender, EventArgs e)
        {
      
        }
        private void mnuStockEnquiry_Click(object sender, EventArgs e)
        {
            //currently generates an namespace error

            frmStockEnquiry frmNext = new frmStockEnquiry(this);
            this.Hide();
            frmNext.Show();
        }

        private void mnuViewOrders_Click(object sender, EventArgs e)
        {
            //currently generates an namespace error

            frmOrderHistory frmNext = new frmOrderHistory(this);
            this.Hide();
            frmNext.Show();
        }


        private void mnuClientRecord_Click(object sender, EventArgs e)
        {
            //currently generates an namespace error

            frmClientRecord frmNext = new frmClientRecord(this);
            this.Hide();
            frmNext.Show();
        }
    }
}
