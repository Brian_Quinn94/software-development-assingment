﻿namespace PcClinic
{
    partial class frmPcClinic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPcClinic));
            this.mnuStrip = new System.Windows.Forms.MenuStrip();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRegStock = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUpdateStock = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStockEnquiry = new System.Windows.Forms.ToolStripMenuItem();
            this.clientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRegClient = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUpdateClient = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuClientRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuViewOrders = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mnuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuStrip
            // 
            this.mnuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockToolStripMenuItem,
            this.clientsToolStripMenuItem,
            this.ordersToolStripMenuItem});
            this.mnuStrip.Location = new System.Drawing.Point(0, 0);
            this.mnuStrip.Name = "mnuStrip";
            this.mnuStrip.Size = new System.Drawing.Size(599, 24);
            this.mnuStrip.TabIndex = 0;
            this.mnuStrip.Text = "menuStrip1";
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuRegStock,
            this.mnuUpdateStock,
            this.mnuStockEnquiry});
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.stockToolStripMenuItem.Text = "Stock";
            // 
            // mnuRegStock
            // 
            this.mnuRegStock.Name = "mnuRegStock";
            this.mnuRegStock.Size = new System.Drawing.Size(148, 22);
            this.mnuRegStock.Text = "Register Stock";
            this.mnuRegStock.Click += new System.EventHandler(this.mnuRegStock_Click);
            // 
            // mnuUpdateStock
            // 
            this.mnuUpdateStock.Name = "mnuUpdateStock";
            this.mnuUpdateStock.Size = new System.Drawing.Size(148, 22);
            this.mnuUpdateStock.Text = "Update Stock";
            this.mnuUpdateStock.Click += new System.EventHandler(this.mnuUpdateStock_Click);
            // 
            // mnuStockEnquiry
            // 
            this.mnuStockEnquiry.Name = "mnuStockEnquiry";
            this.mnuStockEnquiry.Size = new System.Drawing.Size(148, 22);
            this.mnuStockEnquiry.Text = "Stock Enquiry";
            this.mnuStockEnquiry.Click += new System.EventHandler(this.mnuStockEnquiry_Click);
            // 
            // clientsToolStripMenuItem
            // 
            this.clientsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuRegClient,
            this.mnuUpdateClient,
            this.mnuClientRecord});
            this.clientsToolStripMenuItem.Name = "clientsToolStripMenuItem";
            this.clientsToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.clientsToolStripMenuItem.Text = "Clients";
            // 
            // mnuRegClient
            // 
            this.mnuRegClient.Name = "mnuRegClient";
            this.mnuRegClient.Size = new System.Drawing.Size(152, 22);
            this.mnuRegClient.Text = "Register Client";
            this.mnuRegClient.Click += new System.EventHandler(this.mnuRegClient_Click);
            // 
            // mnuUpdateClient
            // 
            this.mnuUpdateClient.Name = "mnuUpdateClient";
            this.mnuUpdateClient.Size = new System.Drawing.Size(152, 22);
            this.mnuUpdateClient.Text = "Update Client";
            this.mnuUpdateClient.Click += new System.EventHandler(this.mnuUpdateClient_Click);
            // 
            // mnuClientRecord
            // 
            this.mnuClientRecord.Name = "mnuClientRecord";
            this.mnuClientRecord.Size = new System.Drawing.Size(152, 22);
            this.mnuClientRecord.Text = "Client Record";
            this.mnuClientRecord.Click += new System.EventHandler(this.mnuClientRecord_Click);
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewOrder,
            this.mnuViewOrders});
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.ordersToolStripMenuItem.Text = "Orders";
            // 
            // mnuNewOrder
            // 
            this.mnuNewOrder.Name = "mnuNewOrder";
            this.mnuNewOrder.Size = new System.Drawing.Size(145, 22);
            this.mnuNewOrder.Text = "New Order";
            this.mnuNewOrder.Click += new System.EventHandler(this.mnuNewOrder_Click);
            // 
            // mnuViewOrders
            // 
            this.mnuViewOrders.Name = "mnuViewOrders";
            this.mnuViewOrders.Size = new System.Drawing.Size(145, 22);
            this.mnuViewOrders.Text = "Order History";
            this.mnuViewOrders.Click += new System.EventHandler(this.mnuViewOrders_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(601, 227);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // frmPcClinic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 252);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.mnuStrip);
            this.MainMenuStrip = this.mnuStrip;
            this.Name = "frmPcClinic";
            this.Text = "PcClinic";
            this.mnuStrip.ResumeLayout(false);
            this.mnuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuStrip;
        private System.Windows.Forms.ToolStripMenuItem stockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuRegStock;
        private System.Windows.Forms.ToolStripMenuItem mnuUpdateStock;
        private System.Windows.Forms.ToolStripMenuItem mnuRegClient;
        private System.Windows.Forms.ToolStripMenuItem mnuUpdateClient;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem mnuClientRecord;
        private System.Windows.Forms.ToolStripMenuItem mnuNewOrder;
        private System.Windows.Forms.ToolStripMenuItem mnuViewOrders;
        private System.Windows.Forms.ToolStripMenuItem mnuStockEnquiry;
    }
}