﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PcClinic.Orders
{
    public partial class frmOrderHistory : Form
    {
        frmPcClinic parent;
        public frmOrderHistory()
        {
            InitializeComponent();
        }
        public frmOrderHistory(frmPcClinic Parent)
        {
            InitializeComponent();
            parent = Parent;
        }


        private void frmConnect_Load(object sender, EventArgs e)
        {
            // create instance of data set
            DataSet ds = new DataSet();

            grdOrder.DataSource = Order.getOrders(ds).Tables["ORDER"];
        }

        private void mnuHome_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
