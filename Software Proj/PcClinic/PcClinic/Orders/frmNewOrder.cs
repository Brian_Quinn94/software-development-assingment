﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PcClinic
{
    public partial class frmNewOrder : Form
    {
        frmPcClinic parent;
        public frmNewOrder()
        {
            InitializeComponent();
        }
        public frmNewOrder(frmPcClinic Parent)
        {
            InitializeComponent();
            parent = Parent;
        }

        private void frmNewOrder_Load(object sender, EventArgs e)
        {
            txtOrder_No.Text = Order.nextOrder_No().ToString("00000");
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {

            Order myOrders = new Order(Convert.ToInt32(txtOrder_No.Text), dtpOrderDate.Value, dtpDueDate.Value);
            MessageBox.Show("Order" + txtOrder_No.Text + "Registered", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dtpOrderDate.Value = DateTime.Now;
            dtpDueDate.Value = DateTime.Now;
            txtOrder_No.Text = Order.nextOrder_No().ToString("00000");

        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmNewOrder_Load_1(object sender, EventArgs e)
        {
            txtOrder_No.Text = Order.nextOrder_No().ToString("00000");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            grpNewOrder.Visible = true;
        }

        private void cboClientID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
