﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using System.Text.RegularExpressions;

namespace PcClinic
{
    public partial class frmRegClient : Form
    {
        frmPcClinic parent;

        public frmRegClient()
        {
            InitializeComponent();
        }

        public frmRegClient(frmPcClinic Parent)
        {
            InitializeComponent();
            parent = Parent;

        }

        private void frmRegClient_Load(object sender, EventArgs e)
        {
            txtClientID.Text = Client.nextClientID().ToString("00000");
        }

        private void btnRegisterClient_Click(object sender, EventArgs e)
        {
            if (txtForname.Text.Equals(""))
            {
                MessageBox.Show("Forname must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtForname.Focus();
                return;
            }
            else if (txtSurname.Text.Equals(""))
            {
                MessageBox.Show("Surname must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSurname.Focus();
                return;
            }
            else if (txtAddress.Text.Equals(""))
            {
                MessageBox.Show("Address must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtAddress.Focus();
                return;
            }



            Client myClients = new Client(Convert.ToInt32(txtClientID.Text), txtForname.Text, txtSurname.Text,
                dtpDob.Value, txtAddress.Text);

            myClients.regClient();
            MessageBox.Show("Stock" + txtClientID.Text + "Registered", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtForname.Text = "";
            txtSurname.Text = "";
            dtpDob.Value = DateTime.Now;
            txtAddress.Text = "";


            txtClientID.Text = Client.nextClientID().ToString("00000");
            txtForname.Focus();

        }

        public Boolean isValidForename(String Forename)
        {

            Regex pattern = new Regex("^[a-zA-Z]+$");

            if (pattern.IsMatch(Forename))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidSurname(String Surname)
        {

            Regex pattern = new Regex("^[a-zA-Z]+$");

            if (pattern.IsMatch(Surname))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidAddress(String Address)
        {

            Regex pattern = new Regex("^[a-zA-Z0-9]+$");

            if (pattern.IsMatch(Address))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mnuHome_Click(object sender, EventArgs e)
        {

            this.Close();
            parent.Visible = true;
        }

        private void txtClientID_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
