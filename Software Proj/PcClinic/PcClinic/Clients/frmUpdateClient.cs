﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using System.Text.RegularExpressions;

namespace PcClinic
{
    
    public partial class frmUpdateClient : Form
    {
        frmPcClinic parent;
        public frmUpdateClient()
        {
            InitializeComponent();
        }
        public frmUpdateClient(frmPcClinic Parent)
        {
            InitializeComponent();
            parent = Parent;
        }

        

        private void btnSearch_Click(object sender, EventArgs e)
        {
            grpUpdateClient.Visible = true;

            txtForname.Focus();
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            //validate data
            if (txtForname.Text.Equals(""))
            {
                MessageBox.Show("Forname must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtForname.Focus();
                return;
            }
            else if (txtSurname.Text.Equals(""))
            {
                MessageBox.Show("Surname must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSurname.Focus();
                return;
            }
            else if (txtAddress.Text.Equals(""))
            {
                MessageBox.Show("Address must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtAddress.Focus();
                return;
            }


            //save in DB
            Client myClient = new Client(int.Parse(cboClient_ID.Text), txtForname.Text.Trim().ToUpper(),
              txtSurname.Text.Trim().ToUpper(), txtAddress.Text.Trim().ToUpper());

            myClient.UpdateClient();
            //Display conf message
            MessageBox.Show("Client " + txtForname.Text.Trim() + ", " + txtSurname.Text.Trim() + 
                "(" + cboClient_ID.Text + ") Updated", "Success", MessageBoxButtons.OK,
                MessageBoxIcon.Information);

            //reset UI
            grpUpdateClient.Visible = false;

            load_Clients();
            cboClient_ID.Focus();

        }

      
        private void UpdateClients_Load(object sender, EventArgs e)
        {
            //load clients from Client file into combo
            load_Clients();
        }
        public void load_Clients()
        {
            //load clients from Client file into combo
            DataTable dt = Client.getClients().Tables[0];

            cboClient_ID.Items.Clear();
            foreach (DataRow row in dt.Rows)
            {
                cboClient_ID.Items.Add(String.Format("{0:0000}", row[0]) + " " + row[1].ToString().Trim() + ", " + row[2]);
            }
        }

        private void cboClient_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            // retrieve client details
            Client aClient = new Client();
            aClient.getClients(Convert.ToInt32(cboClient_ID.Text.Substring(0, 3)));

            //load form controls with instance variable values
            cboClient_ID.Text = aClient.getClient_ID().ToString("0000");
            txtForname.Text = aClient.getForname();
            txtSurname.Text = aClient.getSurname();
           
            
          

            //display client details
            grpUpdateClient.Visible = true;
        }

       
        private void mnuHome_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public Boolean isValidForname(String txtForname)
        {

            Regex pattern = new Regex("^[a-zA-Z]+$");

            if (pattern.IsMatch(txtForname))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidSurname(String txtSurname)
        {

            Regex pattern = new Regex("^[a-zA-Z]+$");

            if (pattern.IsMatch(txtSurname))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidAddress(String txtAddress)
        {

            Regex pattern = new Regex("^[a-zA-Z]+$");

            if (pattern.IsMatch(txtAddress))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void frmUpdateClient_Load(object sender, EventArgs e)
        {

        }
    }
}
