﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PcClinic.Stock
{
    public partial class frmStockEnquiry : Form
    {
        frmPcClinic parent;
        public frmStockEnquiry()
        {
            InitializeComponent();
        }
        public frmStockEnquiry(frmPcClinic Parent)
        {
            InitializeComponent();
            parent = Parent;
        }

        private void frmConnect_Load(object sender, EventArgs e)
        {
            // create instance of data set
            DataSet ds = new DataSet();

            grdStock.DataSource = Stocks.getStock(ds).Tables["STOCK"];
        }



        private void mnuHome_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
