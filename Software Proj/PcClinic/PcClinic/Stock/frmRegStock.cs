﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using System.Text.RegularExpressions;

namespace PcClinic
{

    
    public partial class frmRegStock : Form
    {
        frmPcClinic parent; 
        public frmRegStock()
        {
            InitializeComponent();
       
        }
        public frmRegStock(frmPcClinic Parent)
        {
            InitializeComponent();
            parent = Parent;

        }


        private void btnRegisterStock_Click(object sender, EventArgs e)
        {
            //checks to see if information is in every text boss to continue
            if (txtDescription.Text.Equals(""))
            {
                MessageBox.Show("Description must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDescription.Focus();
                return;
            }
            if (txtPartType.Text.Equals(""))
            {
                MessageBox.Show("Part TYpe must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPartType.Focus();
                return;
            }
            if (txtCostPrice.Text.Equals(""))
            {
                MessageBox.Show("Cost Price must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCostPrice.Focus();
                return;
            }
            if (txtSalesPrice.Text.Equals(""))
            {
                MessageBox.Show("Sales Price must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSalesPrice.Focus();
                return;
            }
            if (txtQuantity.Text.Equals(""))
            {
                MessageBox.Show("Quantity must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtQuantity.Focus();
                return;
            }

            Stocks myStock = new Stocks(Convert.ToInt32(txtStockNo.Text), txtDescription.Text, txtPartType.Text, Convert.ToInt32(txtCostPrice.Text), Convert.ToInt32(txtSalesPrice.Text), Convert.ToInt32(txtQuantity.Text));
            myStock.regStock();
            MessageBox.Show("Stock" + txtStockNo.Text + "Registered", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtDescription.Text = "";
            txtPartType.Text = "";
            txtCostPrice.Text = "";
            txtSalesPrice.Text = "";
            txtQuantity.Text = "";

            txtStockNo.Text = Stocks.nextStockNo().ToString("00000");
            txtDescription.Focus();


        }
        private void mnuReturn_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmRegStock_Load(object sender, EventArgs e)
        {
            txtStockNo.Text = Stocks.nextStockNo().ToString("00000");
        }

        public Boolean isValidDescription(String Description)
        {

            Regex pattern = new Regex("^[a-zA-Z0-9]+$");

            if (pattern.IsMatch(Description))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean isValidPartType(String PartType)
        {

            Regex pattern = new Regex("^[a-zA-Z]+$");

            if (pattern.IsMatch(PartType))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidCostPrice(String CostPrice)
        {

            Regex pattern = new Regex("^[0-9]{10}$");

            if (pattern.IsMatch(CostPrice))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidSalesPrice(String SalesPrice)
        {

            Regex pattern = new Regex("^[0-9]{10}$");

            if (pattern.IsMatch(SalesPrice))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean isValidQuantity(String Quantity)
        {

            Regex pattern = new Regex("^[0-9]{10}$");

            if (pattern.IsMatch(Quantity))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


       
    }
}
