﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using System.Text.RegularExpressions;

namespace PcClinic
{
    
    public partial class frmUpdateStock : Form
    {
        frmPcClinic parent;
        public frmUpdateStock()
        {
            InitializeComponent();
        }
        public frmUpdateStock(frmPcClinic Parent)
        {
            InitializeComponent();
            parent = Parent;
        }

        

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mnuHome_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.Visible = true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            grpUpdateStock.Visible = true;

            txtDescription.Focus();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {   //checks to see if information is in every text boss to continue
            if (txtDescription.Text.Equals(""))
            {
                MessageBox.Show("Description must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDescription.Focus();
                return;
            }
            if (txtPartType.Text.Equals(""))
            {
                MessageBox.Show("Part TYpe must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPartType.Focus();
                return;
            }
            if (txtCostPrice.Text.Equals(""))
            {
                MessageBox.Show("Cost Price must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCostPrice.Focus();
                return;
            }
            if (txtSalesPrice.Text.Equals(""))
            {
                MessageBox.Show("Sales Price must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSalesPrice.Focus();
                return;
            }
            if (txtQuantity.Text.Equals(""))
            {
                MessageBox.Show("Quantity must be entered", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtQuantity.Focus();
                return;
            }


            //save in DB
            Stocks myStock = new Stocks(int.Parse(cboStockNo.Text), txtDescription.Text.Trim().ToUpper(),
              txtPartType.Text.Trim().ToUpper(), (int.Parse(txtCostPrice.Text)), (int.Parse(txtSalesPrice.Text)), (int.Parse(txtQuantity.Text)));

            myStock.UpdateStock();
            //Display conf message
            MessageBox.Show("Stock " + txtDescription.Text.Trim() + ", " + txtPartType.Text.Trim() + ", " + txtCostPrice.Text.Trim()
                + ", " + txtPartType.Text.Trim() + ", " + txtCostPrice.Text.Trim() + ", " + txtSalesPrice.Text.Trim() + 
                "(" + cboStockNo.Text + ") Updated", "Success", MessageBoxButtons.OK,
                MessageBoxIcon.Information);


            //reset UI
            grpUpdateStock.Visible = false;

            load_Stock();
            cboStockNo.Focus();



        }
        private void UpdateStock_Load(object sender, EventArgs e)
        {
            //load clients from Client file into combo
            load_Stock();
        }
        private void cboClient_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            // retrieve client details
            Stocks aStock = new Stocks();
            aStock.getStock(Convert.ToInt32(cboStockNo.Text.Substring(0, 4)));

            //load form controls with instance variable values
            cboStockNo.Text = aStock.getStock_No().ToString("0000");
            txtDescription.Text = aStock.getDescription();
            txtPartType.Text = aStock.getPartType();
           




            //display client details
            grpUpdateStock.Visible = true;
        }
        public void load_Stock()
        {
            //load clients from Client file into combo
            DataTable dt = Stocks.getStock().Tables[0];

            cboStockNo.Items.Clear();
            foreach (DataRow row in dt.Rows)
            {
                cboStockNo.Items.Add(String.Format("{0:0000}", row[0]) + " " + row[1].ToString().Trim() + ", " + row[2]);
            }
        }




        public Boolean isValidDescription(String Description)
        {

            Regex pattern = new Regex("^[a-zA-Z0-9]+$");

            if (pattern.IsMatch(Description))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean isValidPartType(String PartType)
        {

            Regex pattern = new Regex("^[a-zA-Z]+$");

            if (pattern.IsMatch(PartType))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidCostPrice(String CostPrice)
        {

            Regex pattern = new Regex("^[0-9]{10}$");

            if (pattern.IsMatch(CostPrice))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean isValidSalesPrice(String SalesPrice)
        {

            Regex pattern = new Regex("^[0-9]{10}$");

            if (pattern.IsMatch(SalesPrice))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean isValidQuantity(String Quantity)
        {

            Regex pattern = new Regex("^[0-9]{10}$");

            if (pattern.IsMatch(Quantity))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void cboStockNo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
