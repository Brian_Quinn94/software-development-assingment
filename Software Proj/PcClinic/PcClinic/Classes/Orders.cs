﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;



namespace PcClinic
{

    public class Order
    {
        private int Order_No;
        private DateTime Order_Date;
        private DateTime Due_Date;


        public Order()
        {
            Order_No = 0;
            Order_Date = DateTime.Now;
            Due_Date = DateTime.Now;

        }

        public static DataSet getOrders(DataSet ds)
        {

            //Open Connection
            OracleConnection conn = new OracleConnection(DBConnect.oradb);
            conn.Open();
            string strSQL = "SELECT Order_No, Order_Date, Due_Date FROM Order";
            OracleCommand cmd = new OracleCommand(strSQL, conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(ds, "order");
            conn.Close();

            return ds;

        }

        public Order(int Order_No, DateTime Order_Date, DateTime Due_Date)
        {
            Order_No = 0;
            Order_Date = DateTime.Now;
            Due_Date = DateTime.Now;

        }

        public static int nextOrder_No()
        {
            int intNextOrder_No;
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();

            String strlSQL = "SELECT MAX (Order_NO) From Orders";

            OracleCommand cmd = new OracleCommand(strlSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.IsDBNull(0))
                intNextOrder_No = 1;
            else
                intNextOrder_No = Convert.ToInt16(dr.GetValue(0)) + 1;

            myConn.Close();
            return intNextOrder_No;

        }

        public void NewOrder()
        {
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            String strSQL = "INSERT INTO Orders VALUES(" + this.Order_No + ",'" + String.Format("{0:dd-MMM-yyyy}", Order_Date) + "'," +
                    ",'" + String.Format("{0:dd-MMM-yyyy}", Due_Date) + "')";

            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();
            myConn.Close();

        }
        public void UpdateOrder()
        {
            /*
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            //insert strSQL here
            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();
            myConn.Close();
            */
        }



    }
}