﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;


namespace PcClinic
{
     public class Stocks
    {
        private int stock_No;
        private string description;
        private string part_Type;
        private int cost_Price;
        private int sales_Price;
        private int qty;
        public Stocks()
        {
            stock_No = 0;
            description = "";
            part_Type = "";
            cost_Price = 0;
            sales_Price = 0;
            qty = 0;
        }

        public static DataSet getStock(DataSet ds)
        {

            //Open Connection
            OracleConnection conn = new OracleConnection(DBConnect.oradb);
            conn.Open();
            string strSQL = "SELECT Stock_No, Description FROM Stock";
            OracleCommand cmd = new OracleCommand(strSQL, conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(ds, "stock");
            conn.Close();

            return ds;

        }
        public Stocks(int Stock_No, String Description, string Part_Type, int Cost_Price, int Sale_Price, int Qty)
        {
            stock_No = Stock_No;
            description = Description;
            part_Type = Part_Type;
            cost_Price = Cost_Price;
            sales_Price = Sale_Price;
            qty = Qty;

        }
        public int getStock_No() { return stock_No; }
        public String getDescription() { return description; }
        public String getPartType() { return part_Type; }
        public int getCostPrice() { return cost_Price; }
        public int getSalesPrice() { return sales_Price; }
        public int getQty() { return qty; }




        public static int nextStockNo()
        {

            int intNextStockNo;

            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();

            String strlSQL = "SELECT MAX (Stock_No) From Stock";

            OracleCommand cmd = new OracleCommand(strlSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.IsDBNull(0))
                intNextStockNo = 1;
            else
                intNextStockNo = Convert.ToInt16(dr.GetValue(0)) + 1;

            myConn.Close();
            return intNextStockNo;
        }
        public static DataSet getStock()
        {
            // connect to db
            OracleConnection conn = new OracleConnection(DBConnect.oradb);
            conn.Open();
            // Define SQL query
            string strSQL = "SELECT * FROM Stock ORDER BY PART_TYPE, SALE_PRICE";
            OracleCommand cmd = new OracleCommand(strSQL, conn);
            // Execute the query
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "Stock");
            //close database
            conn.Close();
            return ds;
        }

        public void getStock(int Client_ID)
        {
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            string strSQL = "SELECT * FROM STOCK WHERE Stock_No = " + stock_No;
            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();


            dr.Read();
            this.stock_No = dr.GetInt32(0);
            description = dr.GetString(1);
            part_Type = dr.GetString(2);
            cost_Price = dr.GetInt32(3);
            sales_Price = dr.GetInt32(4);
            qty = dr.GetInt32(5);
            
            myConn.Close();
        }

        public void regStock()
        {
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            String strSQL = "INSERT INTO Stock VALUES(" + this.stock_No + ",'" + this.description.ToUpper() + "','" +
            this.part_Type + "'," + this.cost_Price + "," + this.sales_Price + "," + this.qty + ")";

            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();
            myConn.Close();

        }

        public void UpdateStock()
        {
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            String strSQL = "INSERT INTO STOCK VALUES(" + this.stock_No + ",'" + this.description.ToUpper() + "'," +
                     this.part_Type + "'," + this.cost_Price + "," + this.sales_Price + "," + this.qty + ") ";
            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();
            myConn.Close();
        }


    }
}