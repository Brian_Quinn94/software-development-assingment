﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;using Oracle.ManagedDataAccess.Client;

namespace PcClinic
{

    public class Client
    {

        private int Client_ID;
        private string Forename;
        private string Surname;
        private DateTime DoB;
        private string Address;
      
        //setters
        public Client()
        {
            Client_ID = 0;
            Forename = "";
            Surname = "";
            DoB = DateTime.Now;
            Address = "";
        }

        public Client(int Client_ID, String Forname, String Surname, DateTime DoB,String Address)
        {
            Client_ID = 0;
            Forename = "";
            Surname = "";
            DoB = DateTime.Now;
            Address = "";
        }
        public Client(int Client_ID, String Forname, String Surname,  String Address)
        {
            Client_ID = 0;
            Forename = "";
            Surname = "";
            Address = "";
        }

      

        public static DataSet getClient(DataSet ds)
        {

            //Open Connection
            OracleConnection conn = new OracleConnection(DBConnect.oradb);
            conn.Open();
            string strSQL = "SELECT * FROM CLIENT ORDER BY Client_ID Forename, Suranme, DOB, Address ";
            OracleCommand cmd = new OracleCommand(strSQL, conn);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(ds, "client");
            conn.Close();
            
            return ds;

        }

       
        //getters
        public int getClient_ID() { return Client_ID; }
        public String getForname() { return Forename; }
        public String getSurname() { return Surname; }
        public String getAddress() { return Address; }


       


        public static int nextClientID()
        {

            int NextClientID;
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();

            String strlSQL = "SELECT MAX (Client_ID) From Client";

            OracleCommand cmd = new OracleCommand(strlSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.IsDBNull(0))
                NextClientID = 1;
            else
                NextClientID = Convert.ToInt16(dr.GetValue(0)) + 1;

            myConn.Close();
            return NextClientID;

        }

        public static DataSet getClients()
        {
            // connect to db
            OracleConnection conn = new OracleConnection(DBConnect.oradb);
            conn.Open();
            // Define SQL query
            string strSQL = "SELECT * FROM CLIENT ORDER BY Surname, Forename";
            OracleCommand cmd = new OracleCommand(strSQL, conn);
            // Execute the query
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "Client");
            //close database
            conn.Close();
            return ds;
        }



        public void getClients(int Client_ID)
        {
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            string strSQL = "SELECT * FROM CLIENT WHERE Client_ID = " + Client_ID;
            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();


            dr.Read();
            this.Client_ID = dr.GetInt32(0);
            Forename = dr.GetString(1);
            Surname = dr.GetString(2);
            Address = dr.GetString(4);

            myConn.Close();
        }


        public void regClient()
        {

            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();
            String strSQL = "INSERT INTO CLIENT VALUES(" + this.Client_ID + ",'" + this.Forename.ToUpper() + "'," + ",'" + this.Surname.ToUpper() + 
                "'," + ",'" + String.Format("{0:dd-MMM-yyyy}", DoB) + this.Address.ToUpper() + "')";


            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();
            myConn.Close();
        }

        public void UpdateClient()
        {
            
           OracleConnection myConn = new OracleConnection(DBConnect.oradb);
           myConn.Open();
           String strSQL = "UPDATE CLIENT VALUES(" + this.Forename.ToUpper() + "'," + ",'" + this.Surname.ToUpper() +
                "'," + this.Address.ToUpper() + "') WHERE Client_ID ="+ Client_ID;
            OracleCommand cmd = new OracleCommand(strSQL, myConn);
           cmd.ExecuteNonQuery();
           myConn.Close();
           
        }
    }
}