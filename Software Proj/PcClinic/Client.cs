﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace PcClinic
{

    public class Client
    {

        private int Client_ID;
        private string Forename;
        private string Surname;
        private DateTime DoB;
        private string Address;

        public Client()
        {
            Client_ID = 0;
            Forename = "";
            Surname = "";
            DoB = DateTime.Now;
            Address = "";
        }

        public static int nextClientID() {

            int NextClientID;
            OracleConnection myConn = new OracleConnection(DBConnect.oradb);
            myConn.Open();

            String strlSQL = "SELECT MAX (Customer_ID) From Client";

            OracleCommand cmd = new OracleCommand(strlSQL, myConn);
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.IsDBNull(0))
                NextClientID = 1;
            else
                intNextStockNo = Convert.ToInt16(dr.GetValue(0)) + 1;

            myConn.Close();
            return NextClientID;

        }
        public void regClient()
        {

            OracleConnection myConn = new OracleConnection();
            myConn.Open();
            String strSQL = "INSERT INTO Client VALUES(" + this.Client_ID + ",'" + this.Forname.ToUpper() + "'," + ",'" + this.Surname.ToUpper() + "'," + ",'" + this.cost_Price + "'" + this.Address.ToUpper() + "')";

            OracleCommand cmd = new OracleCommand(strSQL, myConn);
            cmd.ExecuteNonQuery();
            myConn.Close();

        }
    }
}
